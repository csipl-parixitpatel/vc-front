import React, { Component } from "react";
import { Image } from 'react-bootstrap';

class Aboutbg extends Component {

    render() {
        return (
            <div>
                <div className="aboutbg">
                    <Image src={process.env.PUBLIC_URL + '/images/about-bg.png'} fluid />
                    <div className="aboutbg-text">
                        <p>At VerificationsCanada, we have teams of highly trained verifications specialists who are passionate about delivering the best service and accurate results every day.
                            It’s why we do what we do!</p>
                        <p>Need more information? Want to start background check services with VerificationsCanada? Call one of our verification specialists today at
                        </p>
                        <a href="tel:1-888-343-3679">1-888-343-3679.</a>
                    </div>
                </div>
            </div>
        );
    }
}

export default Aboutbg;

