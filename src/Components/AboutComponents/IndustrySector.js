import React, { Component } from "react";
import { Container, Row, Col } from 'react-bootstrap';

class IndustrySector extends Component {

    render() {
        return (
            <div>
                <div className="ind-sector">
                    <section>
                        <Container>
                            <Row>
                                <Col md={12}>
                                    <h1 className="color-darkblue text-center">Industry Sector</h1>
                                    <ul>
                                        <li>Accommodations</li>
                                        <li>Accounting</li>
                                        <li>Advertising</li>
                                        <li>Aerospace</li>
                                        <li>Agriculture</li>
                                        <li>Air Transportation</li>
                                        <li>Apparel & Accessories</li>
                                        <li>Auto-Banking</li>
                                        <li>Banking</li>
                                        <li>Beauty & Cosmetics</li>
                                        <li>Biotechnology</li>
                                        <li>Chemical</li>
                                        <li>Communications</li>
                                        <li>Construction</li>
                                        <li>Consulting</li>
                                        <li>Consumer Products</li>
                                        <li>Education</li>
                                        <li>Electronics</li>
                                        <li>Employment</li>
                                        <li>Energy</li>
                                        <li>Entertainment & Recreation</li>
                                        <li>Fashion</li>
                                        <li>Financial Services</li>
                                        <li>Fine Arts</li>
                                        <li>Food & Beverage</li>
                                        <li>Green Technology</li>
                                        <li>Health</li>
                                        <li>Information Technology</li>
                                        <li>Insurance</li>
                                        <li>Journalism & News-Legal Services</li>
                                        <li>Logistics</li>
                                        <li>Manufacturing</li>
                                        <li>Media & Broadcasting</li>
                                        <li>Medical Devices & Supplies</li>
                                        <li>Motion Pictures & Video</li>
                                        <li>Music</li>
                                        <li>Pharmaceutical</li>
                                        <li>Public Administration</li>
                                        <li>Public Relations</li>
                                        <li>Computers</li>
                                        <li>Publishing</li>
                                        <li>Rail</li>
                                        <li>Real Estate</li>
                                        <li>Retail</li>
                                        <li>Service-Sports</li>
                                        <li>Technology</li>
                                        <li>Telecommunications</li>
                                        <li>Tourism</li>
                                        <li>Transportation</li>
                                        <li>Travel</li>
                                        <li>Utilities</li>
                                        <li>Video Gaming</li>
                                    </ul>
                                </Col>
                            </Row>
                        </Container>
                    </section>
                </div>
            </div>
        );
    }
}

export default IndustrySector;

