import React, { Component } from "react";
import { Container, Row, Col, Image, Button } from 'react-bootstrap';

class ProsOfEmployeeBgCheck extends Component {

    render() {
        return (
            <div>
                <div className="pros-check">
                    <Container>
                        <Row>
                            <Col md={6}>
                                <h1 className="color-darkblue">The Pros of Employee <br/>Background Checks</h1>
                                <p>When it comes to hiring potential new employee, pre-employment background checks are becoming more of a requirement than a nice-to-have. There has been many cases of candidates making false claims on job applications and resumes or even make attempts to cover up a criminal past. Approximately 22% of employment background checks performed by VerificationsCanada discover discrepancies.</p>
                                <Button variant="primary">Continue Reading</Button>
                                <p>By partnering with accredited vendors, VerificationsCanada allows you to feel secure and protected with the highest level of safeguarding for you, your potential employee, and your business.</p>
                            </Col>
                            <Col md={6}>
                                <div className="about-chart">
                                    <Image src={process.env.PUBLIC_URL + '/images/about-chart.png'} fluid />
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </div>
            </div>
        );
    }
}

export default ProsOfEmployeeBgCheck;

