import React, { Component } from "react";
import { Helmet } from "react-helmet";
import { Container, Row, Col, Image, Form, Button } from 'react-bootstrap';
import Banner from 'react-js-banner';

class Register extends Component {

    render() {
        return (
            <div>
                <Helmet>
                    <title>Verification Canada | Forgot Password</title>
                </Helmet>
                <Banner
                    className="inner-banner"
                    title="Forgot Password"
                />
                <section>
                    <Container>
                        <div class="reg-form">
                            <h1 className="color-darkblue text-center">Forgot Password</h1><br/>
                            <Row>
                                <Col md={{ span: 6, offset: 3 }}>
                                    <Form.Group>
                                        <Form.Control type="text" placeholder="Eamil Address*" />
                                        <Form.Text className="text-muted">
                                            A link will be sent to the entered email id to change the password.
                                        </Form.Text>
                                    </Form.Group><br/>
                                    <Form.Group className="text-center lastchild">
                                        <Button variant="primary" type="submit">Reset Password</Button>
                                    </Form.Group>
                                </Col>
                            </Row>
                        </div>
                    </Container>
                </section>
            </div>
        );
    }
}

export default Register;
