import React, { Component } from "react";
import { Helmet } from "react-helmet";
import { Container, Row, Col, Image, Form, Button } from 'react-bootstrap';
import Banner from 'react-js-banner';
import Recaptcha from "react-recaptcha";

class Login extends Component {
    constructor(props) {
        super(props);

        this.recaptchaLoaded = this.recaptchaLoaded.bind(this);
    }
    recaptchaLoaded() {
        console.log('Captcha Successfull !')
    }
    render() {
        return (
            <div>
                <Helmet>
                    <title>Verification Canada | Login</title>
                </Helmet>
                <Banner
                    className="inner-banner"
                    title="Login"
                />
                <section>
                    <div class="login">
                        <Container>
                            <Row>
                                <Col xl={5} lg={6} md={6} className="p-0">
                                    <div className="login-img">
                                        <Image src={process.env.PUBLIC_URL + '/images/login-image.png'} fluid />
                                        <div className="login-img-text">
                                            <h2>Login here</h2>
                                        </div>
                                    </div>
                                </Col>
                                <Col xl={7} lg={6} md={6} className="p-0">
                                    <div className="login-form">
                                        <div className="forms">
                                            <Form id="login-form">
                                                <Form.Group>
                                                    <Form.Control type="text" placeholder="User Name" />
                                                </Form.Group>
                                                <Form.Group>
                                                    <Form.Control type="text" placeholder="Password" />
                                                </Form.Group>
                                                <Form.Group className="text-center">
                                                    <Recaptcha
                                                        sitekey="6Lfmya4bAAAAAHf-bK-XrPfCWXGCuX5zKUNyN073"
                                                        render="explicit"
                                                        onloadCallback={this.recaptchaLoaded}
                                                    />
                                                </Form.Group>
                                                <Row>
                                                    <Col md={6}>
                                                        <Form.Group className="mb-3" controlId="formBasicCheckbox">
                                                            <Form.Check type="checkbox" label="Remember Me" />
                                                        </Form.Group>
                                                    </Col>
                                                    <Col md={6}>
                                                        <div class="forgot-pwd">
                                                            <a href="#">Forgot Password ?</a>
                                                        </div>
                                                    </Col>
                                                </Row>
                                                <div className="login-btn">
                                                    <Button variant="primary" type="submit">Sign In</Button>
                                                </div>
                                                <div className="no-acc">
                                                    <p>Don’t have account <a href="#">Register</a></p>
                                                </div>
                                            </Form>
                                        </div>
                                    </div>
                                </Col>
                            </Row>
                        </Container>
                    </div>
                </section>
            </div>
        );
    }
}

export default Login;
