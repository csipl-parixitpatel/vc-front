import React, { Component } from "react";
import { Helmet } from "react-helmet";
import { Container, Row, Col, Image, Form, Button } from 'react-bootstrap';
import Banner from 'react-js-banner';
import Recaptcha from "react-recaptcha";

class Register extends Component {
    constructor(props) {
        super(props);

        this.recaptchaLoaded = this.recaptchaLoaded.bind(this);
    }
    recaptchaLoaded() {
        console.log('Captcha Successfull !')
    }
    render() {
        return (
            <div>
                <Helmet>
                    <title>Verification Canada | Register</title>
                </Helmet>
                <Banner
                    className="inner-banner"
                    title="Account Registration"
                />
                <div className="reg-gradient">
                    <p>Businesses from all over the country depend on VerificationscanadaTM Inc. to supply them with the precise data they need in order to screen potential employees accurately and efficiently to prevent negligent hiring which provides
                        a safer work environment. Please submit your informationone of our representatives will be in touch with you.
                    </p>
                    <a href="tel:Call Toll Free: 1-888-343-3679">Call Toll Free: 1-888-343-3679</a><br />
                    <span>*Note: Registration is for corporate members only</span>
                </div>
                <section>
                    <Container>
                        <div class="reg-form">
                            <h1 className="color-darkblue text-center">Basic Info</h1>
                            <Row>
                                <Col md={6}>
                                    <Form.Group>
                                        <Form.Control type="text" placeholder="Full Name*" />
                                    </Form.Group>
                                </Col>
                                <Col md={6}>
                                    <Form.Group>
                                        <Form.Control type="text" placeholder="Last Name*" />
                                    </Form.Group>
                                </Col>
                                <Col md={6}>
                                    <Form.Group>
                                        <Form.Control type="text" placeholder="Job TItle*" />
                                    </Form.Group>
                                </Col>
                                <Col md={6}>
                                    <Form.Group>
                                        <Form.Control type="text" placeholder="Phone Number*" />
                                    </Form.Group>
                                </Col>
                                <Col md={12}>
                                    <Form.Group controlId="MessageTextarea">
                                        <Form.Control as="textarea" rows={5} placeholder="Email Address*" name="email" />
                                    </Form.Group>
                                </Col>
                            </Row>
                        </div>
                        <div class="reg-form">
                            <h1 className="color-darkblue text-center">ACCOUNT REGISTRATION</h1>
                            <Row>
                                <Col md={6}>
                                    <Form.Group>
                                        <Form.Control type="text" placeholder="Company Name*" />
                                    </Form.Group>
                                </Col>
                                <Col md={6}>
                                    <Form.Group>
                                        <Form.Control as="select">
                                            <option>Headquarter Province</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                        </Form.Control>
                                    </Form.Group>
                                </Col>
                                <Col md={6}>
                                    <Form.Group>
                                        <Form.Control as="select">
                                            <option>Industries</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                        </Form.Control>
                                    </Form.Group>
                                </Col>
                                <Col md={6}>
                                    <Form.Group>
                                        <Form.Control as="select">
                                            <option>Size of Company </option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                        </Form.Control>
                                    </Form.Group>
                                </Col>
                                <Col md={12}>
                                    <Form.Group controlId="MessageTextarea">
                                        <Form.Control as="textarea" rows={5} placeholder="Please Leave Any Additional Comments Regarding Quotes Here...*" name="message" />
                                    </Form.Group>
                                </Col>
                                <Col md={12}>
                                    <Form.Group>
                                        <div className="note">
                                            <p>One of our specialists will contact you within 1 business day to discuss options, offering you the lowest competitive fixed rates for you to start saving right away!</p>
                                        </div>
                                    </Form.Group>
                                </Col>
                            </Row>
                        </div>
                        <div class="reg-form lastchild">
                            <h1 className="color-darkblue text-center">VerificationsCanada Login Credentials</h1>
                            <div className="note">
                                <p className="color-black">Please select your username and password for your VerificationsCanada account. Once you have received your login details
                                    all of your account information will be private, on a secure server and subscription based. Thank you,</p>
                            </div>
                            <Row>
                                <Col md={6}>
                                    <Form.Group>
                                        <Form.Control type="password" placeholder="Enter Password" />
                                    </Form.Group>
                                </Col>
                                <Col md={6}>
                                    <Form.Group>
                                        <Form.Control type="password" placeholder="Confirm Password" />
                                    </Form.Group>
                                </Col>
                                <Col md={12}>
                                    <Form.Group className="text-center">
                                        <Recaptcha
                                            sitekey="6Lfmya4bAAAAAHf-bK-XrPfCWXGCuX5zKUNyN073"
                                            render="explicit"
                                            onloadCallback={this.recaptchaLoaded}
                                        />
                                    </Form.Group>
                                </Col>
                                <Col md={12}>
                                    <Form.Group className="text-center lastchild">
                                        <Button variant="primary" type="submit">Complete Registration</Button>
                                    </Form.Group>
                                </Col>
                            </Row>
                        </div>
                    </Container>
                </section>
                <div className="reg-bottom-text">
                    <p>Processing timelines are dependent on entries and submissions being completed and <span className="color-darkblue">receieved within the hours of 8:30am to 4:30pm Eastern Standard Time,</span> except on holidays.</p>
                </div>
            </div>
        );
    }
}

export default Register;
