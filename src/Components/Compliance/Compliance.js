import { Container, Row, Col, Button } from 'react-bootstrap';
import ComplianceLogos from "../Compliance/ComplianceLogos";

function Compliance() {
    return (
        <div className="compliance">
            <section>
                <Container>
                    <Row>
                        <Col md={7} lg={6}>
                            <h1 className="color-darkblue">Compliance</h1>
                            <ComplianceLogos />
                        </Col>
                        <Col md={5} lg={{ span: 5, offset: 1 }}>
                            <div className="buss-sol">
                                <h1 className="color-black">Business solutions <br /><span className="color-darkblue">Need Regular <br />Background Checks?</span></h1>
                                <p>Need a background check done on a candidate? <br/>We can help you find what’s best for your business.</p>
                                <p>We offer special pricing!</p>
                                <p className="color-darkblue">Official certificates are sent to you in <br/>less than 15 mins to an hour.</p>
                                <Button variant="primary">Get a Quote</Button>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </section>
        </div>
    );
}

export default Compliance;
