import React, { Component } from "react";
import { Row, Col, Image } from 'react-bootstrap';

class ComplianceLogos extends Component {

    render() {
        return (
            <div>
                <div className="comp-logos">
                    <Row>
                        <Col md={6}>
                            <div className="comp-box">
                                <div className="comp-img">
                                    <Image src={process.env.PUBLIC_URL + '/images/aoda.png'} fluid />
                                </div>
                                <h6>AODA</h6>
                            </div>
                        </Col>
                        <Col md={6}>
                            <div className="comp-box">
                                <div className="comp-img">
                                    <Image src={process.env.PUBLIC_URL + '/images/na.png'} fluid />
                                </div>
                                <h6>Partners Are NAPBS<br />Accredited</h6>
                            </div>
                        </Col>
                        <Col md={6}>
                            <div className="comp-box">
                                <div className="comp-img">
                                    <Image src={process.env.PUBLIC_URL + '/images/office.png'} fluid />
                                </div>
                                <h6>PIPEDA</h6>
                            </div>
                        </Col>
                        <Col md={6}>
                            <div className="comp-box">
                                <div className="comp-img">
                                    <Image src={process.env.PUBLIC_URL + '/images/cer.png'} fluid />
                                </div>
                                <h6>SEAL OF APPROVAL</h6>
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}

export default ComplianceLogos;

