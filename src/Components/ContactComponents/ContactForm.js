import ReactFormInputValidation from "react-form-input-validation";
import Recaptcha from "react-recaptcha";
import React from 'react';
import { Row, Col, Form, Button } from 'react-bootstrap';

class ContactForm extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            fields: {
                fname: "",
                message: "",
                email: "",
                phone_number: "",
                cname: "",
            },
            errors: {}
        };
        this.form = new ReactFormInputValidation(this);
        this.form.useRules({
            fname: "required",
            message: "required",
            cname: "required",
            email: "required|email",
            phone_number: "required|numeric|digits_between:10,12",
        });
        this.form.onformsubmit = (fields) => {
            // Do your ajax calls here.
        }

        this.recaptchaLoaded = this.recaptchaLoaded.bind(this);
    }

    recaptchaLoaded(){
        console.log('Captcha Successfull !')
    }

    render() {
        return (
            <div className="contact-form">
                <Form>
                    <h1 className="color-darkblue text-center">We are Happy to Help</h1>
                    <Row>
                        <Col md={12}>
                            <Form.Group controlId="formBasicFname">
                                <Form.Control type="text" placeholder="Full Name" data-attribute-name="first name" value={this.state.fields.fname} onChange={this.form.handleChangeEvent} name="fname" onBlur={this.form.handleBlurEvent} />
                                <span className="error">
                                    {this.state.errors.fname ? this.state.errors.fname : " "}
                                </span>
                            </Form.Group>
                        </Col>
                        <Col md={12}>
                            <Form.Group controlId="formBasicEmail">
                                <Form.Control type="email" placeholder="Email Address" name="email" onBlur={this.form.handleBlurEvent} onChange={this.form.handleChangeEvent} value={this.state.fields.email} />
                                <span className="error">
                                    {this.state.errors.email ? this.state.errors.email : ""}
                                </span>
                            </Form.Group>
                        </Col>
                        <Col md={12}>
                            <Form.Group controlId="formBasicPhone">
                                <Form.Control type="tel" placeholder="Contact Number" data-attribute-name="contact number" name="phone_number" onBlur={this.form.handleBlurEvent} onChange={this.form.handleChangeEvent} value={this.state.fields.phone_number} />
                                <span className="error">
                                    {this.state.errors.phone_number ? this.state.errors.phone_number : ""}
                                </span>
                            </Form.Group>
                        </Col>
                        <Col md={12}>
                            <Form.Group controlId="formBasicCname">
                                <Form.Control type="text" placeholder="Company Name" data-attribute-name="company name" value={this.state.fields.cname} onChange={this.form.handleChangeEvent} name="cname" onBlur={this.form.handleBlurEvent} />
                                <span className="error">
                                    {this.state.errors.cname ? this.state.errors.cname : " "}
                                </span>
                            </Form.Group>
                        </Col>
                        <Col md={12}>
                            <Form.Group controlId="MessageTextarea">
                                <Form.Control as="textarea" rows={5} placeholder="Message" name="message" onBlur={this.form.handleBlurEvent} onChange={this.form.handleChangeEvent} value={this.state.fields.message} />
                                <span className="error">
                                    {this.state.errors.message ? this.state.errors.message : ""}
                                </span>
                            </Form.Group>
                        </Col>
                        <Col md={12}>
                            <Form.Group>
                                <Recaptcha
                                    sitekey="6Lfmya4bAAAAAHf-bK-XrPfCWXGCuX5zKUNyN073"
                                    render="explicit"
                                    onloadCallback={this.recaptchaLoaded}
                                />
                            </Form.Group>
                        </Col>
                        <Col md={12}>
                            <Button variant="outline-primary" type="submit">
                                Send
                            </Button>
                        </Col>
                    </Row>
                </Form>
            </div>
        );
    }
}
export default ContactForm;