import { Row, Col, Image } from 'react-bootstrap';
import React from 'react';

class ContactHere extends React.Component {


    render() {
        return (
            <div className="contact-here">
                <Row>
                    <Col md={4}>
                        <div className="contact-box">
                            <div className="contact-img">
                                <Image src={process.env.PUBLIC_URL + '/images/location.png'} fluid />
                                <h4>Our Location</h4>
                            </div>
                            <p>4 Robert Speck Parkway Suite 1500 Mississauga ON L4Z1S1</p>
                        </div>
                    </Col>
                    <Col md={4}>
                        <div className="contact-box">
                            <div className="contact-img">
                                <Image src={process.env.PUBLIC_URL + '/images/phone.png'} fluid />
                                <h4>Call Us Now</h4>
                            </div>
                            <p><a href="tel:1 888 343 3679">1-888-343-3679</a></p>
                        </div>
                    </Col>
                    <Col md={4}>
                        <div className="contact-box">
                            <div className="contact-img">
                                <Image src={process.env.PUBLIC_URL + '/images/mail.png'} fluid />
                                <h4>Write Us Now</h4>
                            </div>
                            <p><a href="mailto:secure@verificationscanada.ca">secure@verificationscanada.ca</a></p>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}
export default ContactHere;