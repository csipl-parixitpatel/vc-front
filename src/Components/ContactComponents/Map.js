import Iframe from 'react-iframe';
import React from 'react';

class Map extends React.Component {
    

    render() {
        return (
            <div className="g-map">
                <Iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2889.4489598707296!2d-79.63813238450408!3d43.597192879123305!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x882b473b28908bb7%3A0x76c24d515e06133!2s4%20Robert%20Speck%20Pkwy%20%231500%2C%20Mississauga%2C%20ON%20L4Z%201S1%2C%20Canada!5e0!3m2!1sen!2sin!4v1626849828421!5m2!1sen!2sin" style="border:0;" allowfullscreen="" loading="lazy"></Iframe>
            </div>
        );
    }
}
export default Map;