import React, { Component } from "react";
import { Helmet } from "react-helmet";
import { Container, Row, Col, Button, Form } from 'react-bootstrap';
import SideBar from "../Dashboard/SideBar/SideBar";
import Banner from 'react-js-banner';

class AccountDetails extends Component {

    render() {
        return (
            <div>
                <Helmet>
                    <title>Verification Canada | Account Details</title>
                </Helmet>
                <Banner
                    className="inner-banner"
                    title="Account Details"
                />
                <section>
                    <Container>
                        <Row>
                            <Col md={3}>
                                <SideBar />
                            </Col>
                            <Col md={9}>
                                <div className="dashboard-box acc-details">
                                    <Form.Group>
                                        <Form.Label>First Name :<span>*</span></Form.Label>
                                        <Form.Control type="email" placeholder="John" />
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Label>Last Name :<span>*</span></Form.Label>
                                        <Form.Control type="email" placeholder="Doe" />
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Label>Email Address :<span>*</span></Form.Label>
                                        <Form.Control type="email" placeholder="basowa9246@flmmo.com" />
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Label>Confirm Email Address :<span>*</span></Form.Label>
                                        <Form.Control type="email" placeholder="basowa9246@flmmo.com" />
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Label>Phone Number :<span>*</span></Form.Label>
                                        <Form.Control type="email" placeholder="xxx-xxxx-xxxx" />
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Label>Job Title :<span>*</span></Form.Label>
                                        <Form.Control type="email" placeholder="CEO" />
                                    </Form.Group>
                                    <Form.Group>
                                        <Button variant="primary">Save Changes</Button>
                                    </Form.Group>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </section>
            </div>
        );
    }
}

export default AccountDetails;
