import React, { Component } from "react";
import { Helmet } from "react-helmet";
import { Container, Row, Col, Button } from 'react-bootstrap';
import SideBar from "../Dashboard/SideBar/SideBar";
import Banner from 'react-js-banner';

class Invoice extends Component {

    render() {
        return (
            <div>
                <Helmet>
                    <title>Verification Canada | Addresses</title>
                </Helmet>
                <Banner
                    className="inner-banner"
                    title="Addresses"
                />
                <section>
                    <Container>
                        <Row>
                            <Col md={3}>
                                <SideBar />
                            </Col>
                            <Col md={9}>
                                <div className="dashboard-box address">
                                    <h5>The following addresses will be used on the checkout page by default.</h5>
                                    <Row>
                                        <Col md={5}>
                                            <h4>Billing Address</h4>
                                            <p>Chuck Johnson<br/>378 Young St Truro NS B2N 7H2<br/>3443 Victoria Park Ave<br/>Phone:  (902) 843-8023</p>
                                        </Col>
                                        <Col md={5}>
                                            <h4>Shipping Address</h4>
                                            <p>Chuck Johnson<br/>378 Young St Truro NS B2N 7H2<br/>3443 Victoria Park Ave<br/>Phone:  (902) 843-8023</p>
                                        </Col>
                                    </Row><br/><br/>
                                    <Button variant="primary">Change Address</Button>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </section>
            </div>
        );
    }
}

export default Invoice;
