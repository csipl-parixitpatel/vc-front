import React, { Component } from "react";
import { Helmet } from "react-helmet";
import { Container, Row, Col, Button, Form } from 'react-bootstrap';
import SideBar from "../Dashboard/SideBar/SideBar";
import Banner from 'react-js-banner';

class ChangePassword extends Component {

    render() {
        return (
            <div>
                <Helmet>
                    <title>Verification Canada | Change Password</title>
                </Helmet>
                <Banner
                    className="inner-banner"
                    title="Change Password"
                />
                <section>
                    <Container>
                        <Row>
                            <Col md={3}>
                                <SideBar />
                            </Col>
                            <Col md={9}>
                                <div className="dashboard-box acc-details">
                                    <Form.Group>
                                        <Form.Label>Current Password (Leave blank to leave unchanged) :</Form.Label>
                                        <Form.Control type="email" />
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Label>New Password (Leave blank to leave unchanged) :</Form.Label>
                                        <Form.Control type="email" />
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Label>Confirm New Password</Form.Label>
                                        <Form.Control type="email" />
                                    </Form.Group>
                                    <Form.Group>
                                        <Button variant="primary">Change Password</Button>
                                    </Form.Group>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </section>
            </div>
        );
    }
}

export default ChangePassword;
