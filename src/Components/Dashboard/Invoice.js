import React, { Component } from "react";
import { Helmet } from "react-helmet";
import { Container, Row, Col, Table } from 'react-bootstrap';
import SideBar from "../Dashboard/SideBar/SideBar";
import Banner from 'react-js-banner';

class Invoice extends Component {

    render() {
        return (
            <div>
                <Helmet>
                    <title>Verification Canada | Orders</title>
                </Helmet>
                <Banner
                    className="inner-banner"
                    title="Invoice"
                />
                <section>
                    <Container>
                        <Row>
                            <Col md={3}>
                                <SideBar />
                            </Col>
                            <Col md={9}>
                                <div className="orders">
                                    <Table striped>
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Form Check</th>
                                                <th>Invoice</th>
                                                <th>Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>John</td>
                                                <td>Criminal</td>
                                                <td>Jul Mon, 2021</td>
                                                <td><a href="#">#2609</a></td>
                                            </tr>
                                            <tr>
                                                <td>Rock</td>
                                                <td>Criminal</td>
                                                <td>Jul Mon, 2021</td>
                                                <td><a href="#">#2609</a></td>
                                            </tr>
                                        </tbody>
                                    </Table>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </section>
            </div>
        );
    }
}

export default Invoice;
