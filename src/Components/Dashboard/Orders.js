import React, { Component } from "react";
import { Helmet } from "react-helmet";
import { Container, Row, Col, Table } from 'react-bootstrap';
import SideBar from "../Dashboard/SideBar/SideBar";
import Banner from 'react-js-banner';

class Orders extends Component {

    render() {
        return (
            <div>
                <Helmet>
                    <title>Verification Canada | Orders</title>
                </Helmet>
                <Banner
                    className="inner-banner"
                    title="Orders"
                />
                <section>
                    <Container>
                        <Row>
                            <Col md={3}>
                                <SideBar />
                            </Col>
                            <Col md={9}>
                                <div className="orders table-responsive">
                                    <Table striped>
                                        <thead>
                                            <tr>
                                                <th>Order</th>
                                                <th>Form Check</th>
                                                <th>Date</th>
                                                <th>Order Status</th>
                                                <th>Payment Status</th>
                                                <th>Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>#2609</td>
                                                <td>Criminal</td>
                                                <td>Jul Mon, 2021</td>
                                                <td>Completed</td>
                                                <td>Confirm</td>
                                                <td>$0.79</td>
                                            </tr>
                                            <tr>
                                                <td>#2609</td>
                                                <td>Criminal</td>
                                                <td>Jul Mon, 2021</td>
                                                <td>Completed</td>
                                                <td>Confirm</td>
                                                <td>$0.79</td>
                                            </tr>
                                        </tbody>
                                    </Table>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </section>
            </div>
        );
    }
}

export default Orders;
