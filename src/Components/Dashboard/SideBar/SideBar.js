import React, { Component } from "react";
import { NavLink } from 'react-router-dom';
import { Image, Nav } from 'react-bootstrap';


class SideBar extends Component {

    render() {
        return (
            <div className="sidebar">
                <Nav>
                    <Nav.Link to="" className="firstchild" eventKey="disabled" disabled>Welcome John Doe</Nav.Link>
                    <NavLink to="/Orders" activeClassName="active">
                        <Image src={process.env.PUBLIC_URL + '/images/icon-order.png'} fluid />&nbsp;Orders
                    </NavLink>
                    <NavLink to="/Invoice" activeClassName="active">
                        <Image src={process.env.PUBLIC_URL + '/images/icon-invoice.png'} fluid />&nbsp;Invoice
                    </NavLink>
                    <NavLink to="/Addresses" activeClassName="active">
                        <Image src={process.env.PUBLIC_URL + '/images/icon-location.png'} fluid />&nbsp;Addresses
                    </NavLink>
                    <NavLink to="/AccountDetails" activeClassName="active">
                        <Image src={process.env.PUBLIC_URL + '/images/icon-user.png'} fluid />&nbsp;Account Details
                    </NavLink>
                    <NavLink to="/ChangePassword" activeClassName="active">
                        <Image src={process.env.PUBLIC_URL + '/images/icon-lock.png'} fluid />&nbsp;Change Password
                    </NavLink>
                    <NavLink to="" className="no-active">
                        <Image src={process.env.PUBLIC_URL + '/images/icon-logout.png'} fluid />&nbsp;Logout
                    </NavLink>
                </Nav>
            </div>
        );
    }
}

export default SideBar;
