import { Container, Row, Col, Button } from 'react-bootstrap';

function FollowSteps() {
    return (
        <div className="follow">
            <section>
                <h1 className="text-center color-white">FOLLOW 4<br />EASY STEPS</h1>
                <div className="numbers">
                    <Container>
                        <Row>
                            <Col md={3} xs={6}>
                                <div className="num">
                                    <h5>1</h5>
                                    <p>Select<br /><span>Service</span></p>
                                </div>
                            </Col>
                            <Col md={3} xs={6}>
                                <div className="num">
                                    <h5>2</h5>
                                    <p><span>Complete</span><br />Application</p>
                                </div>
                            </Col>
                            <Col md={3} xs={6}>
                                <div className="num">
                                    <h5>3</h5>
                                    <p>The<br /><span>Search</span></p>
                                </div>
                            </Col>
                            <Col md={3} xs={6}>
                                <div className="num">
                                    <h5>4</h5>
                                    <p><span>Your</span><br />Result</p>
                                </div>
                            </Col>
                        </Row>
                        <div className="follow-btn text-center">
                            <Button variant="primary">Get Started</Button>
                        </div>
                    </Container>
                </div>
            </section>
        </div>
    );
}

export default FollowSteps;
