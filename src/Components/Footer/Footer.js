import React, { Component } from "react";
import { Container, Row, Col, Image } from 'react-bootstrap';

class Footer extends Component {

  render() {
    return (
      <div>
        <footer>
          <Container>
            <Row>
              <Col md={12}>
                <div className="footer-logo">
                  <Image src={process.env.PUBLIC_URL + '/images/footer-logo.png'} fluid />
                </div>
                <div className="footer-links">
                  <ul>
                    <li><a href="#">Standard Criminal Record Check</a></li>
                    <li><a href="#">Enhanced Criminal Record Check </a></li>
                    <li><a href="#">International Criminal Record Check</a></li>
                    <li><a href="#">US Criminal Background Check</a></li>
                    <li className="lastchild"><a href="#">Softcheck</a></li>
                    <li><a href="#">Education Verification Check</a></li>
                    <li><a href="#">Employment Verification Check</a></li>
                    <li className="lastchild"><a href="#">Motor Vehicle Record Check</a></li>
                  </ul>
                </div>
              </Col>
            </Row>
          </Container>
          <hr />
          <Container>
            <Row>
              <Col md={12}>
                <div className="other-links">
                  <ul>
                    <li><a href="#">Get a Quote</a></li>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Contact</a></li>
                  </ul>
                </div>
              </Col>
            </Row>
          </Container>
          <hr />
          <Container>
            <Row>
              <Col md={12}>
                <div className="other-links">
                  <p>Address: 4 Robert Speck Parkway, Suite 1500 Mississauga, ON L4Z 1S1</p>
                </div>
              </Col>
            </Row>
          </Container>
          <hr />
          <Container>
            <Row>
              <Col md={12}>
                <p>© Copyrights 2020 Kingmaker.  |  Powered by <a href="#"><span class="left-angle">&lt;</span>&nbsp;cs&nbsp;<span class="right-angle">&gt;</span></a></p>
              </Col>
            </Row>
          </Container>
        </footer>
      </div>
    );
  }
}

export default Footer;

