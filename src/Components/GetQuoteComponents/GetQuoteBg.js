import React, { Component } from "react";
import { Container, Row, Col, Image, Button } from 'react-bootstrap';

class GetQuoteBg extends Component {

    render() {
        return (
            <div>
                <div className="quote-bg">
                    <p>Our Consultants are looking forward to tell you more about our services and pricing.</p>
                    <a className="btn btn-primary" href="#">get a quote</a>
                </div>
            </div>
        );
    }
}

export default GetQuoteBg;

