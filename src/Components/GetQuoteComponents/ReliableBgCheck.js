import { Container, Row, Col, Image, Button } from 'react-bootstrap';

function ReliableBgCheck() {
    return (
        <div className="why-vc quote-main">
            <Container>
                <Row>
                    <Col md={12}>
                        <div className="why quote-box">
                            <h1 className="text-center color-darkblue">Reliable Background Checks Services <br/>for Your Business</h1>
                            <Row>
                                <Col lg={{ span: 5, offset: 1 }} md={6}>
                                    <div className="why-box quote-box-text">
                                        <Image src={process.env.PUBLIC_URL + '/images/q-icon1.png'} fluid />
                                        <div class="block-class">
                                            <h6>We Are Online</h6>
                                            <p>No paper, no faxes, no lineups, we do it all online for you</p>
                                        </div>
                                    </div>
                                </Col>
                                <Col lg={{ span: 5, offset: 1 }} md={6}>
                                    <div className="why-box quote-box-text">
                                        <Image src={process.env.PUBLIC_URL + '/images/q-icon2.png'} fluid />
                                        <div class="block-class">
                                            <h6>We Save Your Cost</h6>
                                            <p>We are 20% lower than the competition with no hidden fees</p>
                                        </div>
                                    </div>
                                </Col>
                                <Col lg={{ span: 5, offset: 1 }} md={6}>
                                    <div className="why-box quote-box-text">
                                        <Image src={process.env.PUBLIC_URL + '/images/q-icon3.png'} fluid />
                                        <div class="block-class">
                                            <h6>We Are Secure</h6>
                                            <p>We take your security and privacy seriously</p>
                                        </div>
                                    </div>
                                </Col>
                                <Col lg={{ span: 5, offset: 1 }} md={6}>
                                    <div className="why-box quote-box-text">
                                        <Image src={process.env.PUBLIC_URL + '/images/q-icon4.png'} fluid />
                                        <div class="block-class">
                                            <h6>We Are On Time</h6>
                                            <p>We value your time and prioritize your request</p>
                                        </div>
                                    </div>
                                </Col>
                            </Row>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}

export default ReliableBgCheck;
