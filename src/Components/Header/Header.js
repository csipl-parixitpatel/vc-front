import React, { Component } from "react";
import { Container, Image, Row, Col, Button, Dropdown } from 'react-bootstrap';

class Header extends Component {

  render() {
    return (
      <div>
        <header>
          <Container fluid>
            <Row>
              <Col md={5}>
                <div className="logo">
                  <a href="#">VerificationsWorldwide.ca</a>
                </div>
              </Col>
              <Col md={7}>
                <div class="nav-right">
                  <Button variant="primary" className="gradient-btn">Get Started</Button>
                    <Dropdown>
                      <Dropdown.Toggle variant="primary" id="dropdown-basic" className="menu-drop">
                        <Image src={process.env.PUBLIC_URL + '/images/menu-dropdown.png'} fluid />
                      </Dropdown.Toggle>
                      <Dropdown.Menu>
                        <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                        <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
                        <Dropdown.Item href="#/action-3">Something else</Dropdown.Item>
                      </Dropdown.Menu>
                    </Dropdown>
                    <Dropdown>
                      <Dropdown.Toggle variant="primary" id="dropdown-basic" className="lang-drop">
                        <Image src={process.env.PUBLIC_URL + '/images/globe.png'} fluid />&nbsp;Languages
                      </Dropdown.Toggle>
                      <Dropdown.Menu>
                        <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                        <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
                        <Dropdown.Item href="#/action-3">Something else</Dropdown.Item>
                      </Dropdown.Menu>
                    </Dropdown>
                </div>

              </Col>
            </Row>
          </Container>
        </header>
      </div>
    );
  }
}

export default Header;

