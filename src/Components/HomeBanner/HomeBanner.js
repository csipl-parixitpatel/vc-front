import React, { Component } from "react";
import { Carousel, Image, Button, Row, Col } from 'react-bootstrap';

class HomeBanner extends Component {

    render() {
        return (
            <div className="home-banner">
                <Carousel>
                    <Carousel.Item>
                        <div className="banner-img">
                            <Image
                                className="d-block w-100"
                                src={process.env.PUBLIC_URL + '/images/banner.png'}
                                alt="First slide"
                            />
                        </div>
                        <Carousel.Caption>
                            <Image src={process.env.PUBLIC_URL + '/images/logo.png'} fluid />
                            <p>We Provide Certified Police Record Checks</p>
                            <h2>Get Your Police Criminal<br />Record Or Background<br />Check.. <span>We Also Provide International Criminal Checks!</span></h2>
                            <p>Right Here, Right Now!</p>
                            <h4>Follow 4 simple steps & get your<br />
                                Canadian background check<br />
                                within <span>15 minutes</span></h4>
                            <Row>
                                <Col lg={3} md={6}>
                                    <Button variant="primary">Get my check now</Button>
                                    <h5>Only <span>$49.95</span></h5>
                                </Col>
                            </Row>
                        </Carousel.Caption>
                    </Carousel.Item>
                </Carousel>
            </div>
        );
    }
}

export default HomeBanner;

