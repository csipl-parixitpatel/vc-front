import { Container, Row, Col, Image } from 'react-bootstrap';
import React from "react";
import OwlCarousel from "react-owl-carousel";
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";

class OurPartners extends React.Component {
    state = {
        nav: true,
        navText:["<div class='nav-btn prev-slide'></div>","<div class='nav-btn next-slide'></div>"],
        responsive: {
            0: {
                items: 1,
            },
            450: {
                items: 1,
            },
            600: {
                items: 1,
            },
            1000: {
                items: 1,
            },
        },
    }

    render() {
        return (
            <section>
                <div className="our-partners">
                    <Container fluid>
                        <Row>
                            <Col lg={6} md={6}>
                                <h1 className="color-darkblue">OUR PARTNERS</h1>
                                <div className="partner-logos">
                                    <OwlCarousel className="owl-theme" loop margin={10} nav responsive={this.state.responsive} autoplay={true}>
                                        <div className="item">
                                            <Row>
                                                <Col md={6}>
                                                    <div className="p-logo">
                                                        <Image src={process.env.PUBLIC_URL + '/images/sterling.png'} fluid />
                                                    </div>
                                                </Col>
                                                <Col md={6}>
                                                    <div className="p-logo">
                                                        <Image src={process.env.PUBLIC_URL + '/images/dash.png'} fluid />
                                                    </div>
                                                </Col>
                                                <Col md={6}>
                                                    <div className="p-logo">
                                                        <Image src={process.env.PUBLIC_URL + '/images/grazil.png'} fluid />
                                                    </div>
                                                </Col>
                                                <Col md={6}>
                                                    <div className="p-logo">
                                                        <Image src={process.env.PUBLIC_URL + '/images/decision.png'} fluid />
                                                    </div>
                                                </Col>
                                            </Row>
                                        </div>
                                        <div className="item">
                                            <Row>
                                                <Col md={6}>
                                                    <div className="p-logo">
                                                        <Image src={process.env.PUBLIC_URL + '/images/sterling.png'} fluid />
                                                    </div>
                                                </Col>
                                                <Col md={6}>
                                                    <div className="p-logo">
                                                        <Image src={process.env.PUBLIC_URL + '/images/dash.png'} fluid />
                                                    </div>
                                                </Col>
                                                <Col md={6}>
                                                    <div className="p-logo">
                                                        <Image src={process.env.PUBLIC_URL + '/images/grazil.png'} fluid />
                                                    </div>
                                                </Col>
                                                <Col md={6}>
                                                    <div className="p-logo">
                                                        <Image src={process.env.PUBLIC_URL + '/images/decision.png'} fluid />
                                                    </div>
                                                </Col>
                                            </Row>
                                        </div>
                                    </OwlCarousel>
                                </div>
                            </Col>
                            <Col lg={{ span: 5, offset: 1 }} md={6}>
                                <div className="testimonial">
                                <OwlCarousel className="owl-theme" loop margin={10} nav responsive={this.state.responsive} autoplay={true}>
                                        <div className="item">
                                            <div class="testi-quote">
                                                <Image src={process.env.PUBLIC_URL + '/images/quote.png'} fluid />
                                            </div>
                                            <p>To buy sell and trade never became easy for our users until we partnered with VerificationsCanada. No other Classifieds will compare to ours now that we have verified users as our main criteria and compliance. Thanks to their team we will continue to use their services as we becomes Canada's #1 Veri</p>
                                            <h4>Coneisha Mclean</h4>
                                            <h5>President & CEO</h5>
                                            <h6>Grazil, Free Advertising Made Easy</h6>
                                        </div>
                                        <div className="item">
                                            <div class="testi-quote">
                                                <Image src={process.env.PUBLIC_URL + '/images/quote.png'} fluid />
                                            </div>
                                            <p>To buy sell and trade never became easy for our users until we partnered with VerificationsCanada. No other Classifieds will compare to ours now that we have verified users as our main criteria and compliance. Thanks to their team we will continue to use their services as we becomes Canada's #1 Veri</p>
                                            <h4>Coneisha Mclean</h4>
                                            <h5>President & CEO</h5>
                                            <h6>Grazil, Free Advertising Made Easy</h6>
                                        </div>
                                    </OwlCarousel>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </div>
            </section>
        );
    }
}

export default OurPartners;
