import React, { Component } from "react";
import { Helmet } from "react-helmet";
import { Container, Row, Col, Image } from 'react-bootstrap';
import Banner from 'react-js-banner';
import ComplianceLogos from "../Compliance/ComplianceLogos";
import Aboutbg from "../AboutComponents/Aboutbg";
import ProsOfEmployeeBgCheck from "../AboutComponents/ProsOfEmployeeBgCheck";
import IndustrySector from "../AboutComponents/IndustrySector";

class About extends Component {
    render() {
        return (
            <div>
                <Helmet>
                    <title>Verification Canada | About Us</title>
                </Helmet>
                <Banner 
                    className="inner-banner"
                    title="About Us"
                />
                <section>
                    <div className="about-content">
                        <h1 className="color-darkblue text-center">About VerificationsCanada</h1>
                        <Container>
                            <div className="about-section1">
                                <Row>
                                    <Col lg={6}>
                                        <Image src={process.env.PUBLIC_URL + '/images/about-image1.png'} fluid />
                                    </Col>
                                    <Col lg={6}>
                                        <p>VerificationsCanada is Canada's verification specialist and secure online background check services company, specializing in certified criminal checks, enhanced criminal checks, advanced fingerprinting service, refrence checks, employment verification, credit checks and tenant screening. VerificationsCanada is a proud canadian based company that establishes and solidifies trust with each and every
                                            client we work with, no matter what background check service we
                                            provide.</p>
                                        <p>Since 2014, VerificationsCanada has been a vital extension of the Human Resources department for Canadian companies that require high quality employment screening and hiring solutions so that they can feel confident and trusted in the hiring decisions they make. With online top of the line series across Canada, we maintain unparalleled customer service coupled with impeccable results and an unmatched turnaround time for all background check services offered.</p>
                                    </Col>
                                </Row>
                            </div>
                            <div className="about-section2">
                                <Row>
                                    <Col lg={6}>
                                        <p>VerificationsCanada ensures that our clients are receiving background check results from the highest level of security and law enforcement. We adhere to the strictest standards in the appropriate disclosure of personal information and complaince with the following:</p>
                                        <p>Canadian Code of Practice for Consumer Protection in Electronic Commerce, accredited by Canadian Criminal Real Time Identification Services (CCRTIS)</p>
                                        <ComplianceLogos />
                                    </Col>
                                    <Col lg={6}>
                                        <div className="section2-img">
                                            <Image src={process.env.PUBLIC_URL + '/images/about-image2.png'} fluid />
                                        </div>
                                    </Col>
                                </Row>
                            </div>
                            <div className="about-section3">
                                <Row>
                                    <Col lg={6}>
                                        <Image src={process.env.PUBLIC_URL + '/images/about-image3.png'} fluid />
                                    </Col>
                                    <Col lg={6}>
                                        <p>With VerificationsCanada there is no paper, no faxing, no busy signals and no more waiting in line! We provide all our background check services online where our clients gain access from the comfort of their own home or place of business. The VerificationsCanada secure authentication platform is easy-to-use, fast, dependable, and most importantly will deliver results. By using this unique business model, VerificationsCanada is cost-effective by offering the same background check services as our competitors at a lower rate by up to 20%.</p>
                                        <p>VerificationsCanada is also the leader in growth and evolution of the background check services industry. We offer customized hiring solutions through our loyal partnerships with global leaders in digital security, identification, authentication and biometric technologies. We are connected to the largest forensic biometric database in world, which is used by the Federal Bureau of Investigation and Canadian Security Intelligence Service. As technologies in the background check services industry continue to change, VerificationsCanada remains innovative and adaptable to evolution of the field.</p>
                                    </Col>
                                </Row>
                            </div>
                        </Container>
                    </div>
                </section>
                <Aboutbg />
                <section>
                    <ProsOfEmployeeBgCheck />
                </section>
                <IndustrySector />
            </div>
        );
    }
}

export default About;
