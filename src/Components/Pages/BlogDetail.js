import React, { Component } from "react";
import { Container, Row, Col, Image } from 'react-bootstrap';
import { Helmet } from "react-helmet";
import Banner from 'react-js-banner';
import RecentBlogs from '../RecentBlogs/RecentBlogs';

class BlogDetail extends Component {

    render() {
        return (
            <div>
                <Helmet>
                    <title>Verification Canada | Blog Detail</title>
                </Helmet>
                <Banner
                    className="inner-banner"
                    title="The Ultimate Guide To Employment Verification Check"
                />
                <section>
                    <div className="blog-detail">
                        <Container>
                            <Row>
                                <Col lg={6}>
                                    <div className="blog-detail-img">
                                        <Image src={process.env.PUBLIC_URL + '/images/blog-detail.png'} fluid />
                                    </div>
                                </Col>
                                <Col lg={6}>
                                    <h1>The Ultimate Guide To Employment Verification Check</h1>
                                    <p>Hiring a suitable candidate from a large pool of candidates is a nerve-wracking affair. Nerve-wracking because candidates won't mention anything negative in the resume. And the last thing an employer wants is to bid goodbye to its staff for wrong reasons.</p>
                                    <p>More importantly, hiring an employee with a criminal record may also spell trouble for the employer. Popular job search website CareerBuilder, in 2014, found that 60% of candidates had fabricated details in their resumes to appear professionally attractive.</p>
                                    <p>Hence, spending a little on a criminal background check company at any stage is an absolute must for saving both time and money in the future.</p>
                                    <p>Let's delve deep into the steps and formalities required for a check.</p>
                                </Col>
                                <Col lg={12}>
                                    <h1><span className="color-darkblue">Step 1</span> - Take the Candidate's Written Consent</h1>
                                    <p>The first requirement for conducting an employment check is to make the candidate sign an Employment Verification Consent Form. The Consent Form usually contains a declaration, along with sections like name,  address, and employee ID of the candidate, and name and address of the previous employer.</p>
                                    <p>As per current industry standards, employers obtain information about the previous five years' employment history.</p>
                                    <br />
                                    <h1><span className="color-darkblue">Step 2</span> - Investigate Criminal Records</h1>
                                    <p>The second step in the employment verification process is a thorough analysis of criminal records. For collecting such information, the employer should refer to state and police records, and cases with federal courts involving the candidate.</p>
                                    <p>For example, an applicant needs to submit documents like identity proof, photograph, signature, and a processing fee, other than dropping or picking up the letter in person, for a criminal record check in Toronto. However, all of these can be time consuming and painstaking.</p>
                                    <p>This is where professional service providers come to the rescue. They liaision with government officials, investigate the records, and explain to the client.</p>
                                    <br />
                                    <h1><span className="color-darkblue">Step 3</span> - Identity Status Authorization</h1>
                                    <p>Keeping federal laws in mind, an employer should hire foreigners only if they meet legal criteria for employment. A Canadian citizen, however, in most cases, requires a Social Security Number (SSN) to apply for jobs.</p>
                                    <p>There have been cases where candidates fudged details to appear authentic. To avoid hassles later, verifying the authenticity of a candidate through legitimate channels is a must-do exercise before recruiting.</p>
                                    <br />
                                    <h1><span className="color-darkblue">Step 4</span> - Report on Credit History</h1>
                                    <p>The credit history of a candidate paints a detailed picture of the financial health of the candidate. Through credit score, an employer may understand whether the candidate had ever indulged in financial malpractices or not.</p>
                                    <p>However, taking the written consent of the candidate is mandatory for conducting such checks. In Canada, TransUnion and Equifax are the two independent agencies that generate credit scores based on a person's spending, borrowing, and repayment records.</p>
                                    <br />
                                    <h1><span className="color-darkblue">Step 4</span> - Reference Check</h1>
                                    <p>Typical details that can be obtained from the previous employer include tenure and status of employment, gross salary, job title, and employer's observations. Some organizations have a well laid out process for disclosing information.</p>
                                    <p>You need to draft a request letter containing all relevant details of the employee and send it to the HR wing of concerned employers. Usually, they reply anytime between 7 days and a month. For organizations that don't have such a process, it's better to call or send someone to visit in person.</p>
                                    <br /><br />
                                    <h1><span className="color-darkblue">Conclusion</span></h1>
                                    <p>In addition to the steps discussed, it is also important to verify the authenticity of the educational and other relevant certifications. Some employers even go to the extent of checking a candidate's social media behaviour.</p>
                                    <p>VerificationsCanada is a premier verification services provider, providing services like background check, fingerprinting services, employment verification, and criminal record searches in Canada. All reports generated by VerificationsCanada adhere to CCRTIS, CPIC,  AODA, and PIPEDA standards. All their services are online, convenient, and fast.</p>
                                    <p>They have special access to the world's largest biometric database used by the likes of the FBI and CSIS. And their loyal partnerships with leading business houses enable them to provide services at a 20% cheaper rate than other providers.</p>
                                </Col>
                            </Row>
                        </Container>
                    </div>
                    <RecentBlogs />
                </section>
            </div>
        );
    }
}

export default BlogDetail;
