import React, { Component } from "react";
import { Container, Row, Col, Image } from 'react-bootstrap';
import { Helmet } from "react-helmet";
import Banner from 'react-js-banner';


class BlogListing extends Component {
    render() {
        return (
            <div>
                <Helmet>
                    <title>Verification Canada | Blog Listing</title>
                </Helmet>
                <Banner
                    className="inner-banner"
                    title="Blog"
                />
                <section>
                    <div className="blog-listing">
                        <Container>
                            <Row>
                                <Col md={6}>
                                    <div className="blog-box">
                                        <div className="blog-img">
                                            <Image src={process.env.PUBLIC_URL + '/images/blog1.png'} fluid />
                                        </div>
                                        <div className="blog-box-text">
                                            <h4>The Ultimate Guide To Employment Verification Chec...</h4>
                                            <div className="border-class">
                                                <span>By <strong>Admin</strong></span><br/>
                                                <span>November 06, 2019</span>
                                            </div>
                                            <p>Hiring a suitable candidate from a large pool of candidates is a nerve-wracking affair. Nerve-wracking because candidates won't mention anything negative in the resume. And the last thing an employer ...</p>
                                            <a className="btn btn-primary" href="#">Read More</a>
                                        </div>
                                    </div>
                                </Col>
                                <Col md={6}>
                                    <div className="blog-box">
                                        <div className="blog-img">
                                            <Image src={process.env.PUBLIC_URL + '/images/blog2.png'} fluid />
                                        </div>
                                        <div className="blog-box-text">
                                            <h4>Fingerprints Background Checks – What You Need T...</h4>
                                            <div className="border-class">
                                                <span>By <strong>Admin</strong></span><br/>
                                                <span>November 06, 2019</span>
                                            </div>
                                            <p>In a fingerprint background check, fingerprint data is required to match a person to a criminal record. Today, almost every industry and employer prefers using some form of background check or screeni...</p>
                                            <a className="btn btn-primary" href="#">Read More</a>
                                        </div>
                                    </div>
                                </Col>
                                <Col md={6}>
                                    <div className="blog-box">
                                        <div className="blog-img">
                                            <Image src={process.env.PUBLIC_URL + '/images/blog3.png'} fluid />
                                        </div>
                                        <div className="blog-box-text">
                                            <h4>Navigating Process Of Employee Background Check...</h4>
                                            <div className="border-class">
                                                <span>By <strong>Admin</strong></span><br/>
                                                <span>November 06, 2019</span>
                                            </div>
                                            <p>Getting a wrong employee can be very costly for you, the existing members of staff, and your entire company. An employee background check in this context is important since it allows you to check and ...</p>
                                            <a className="btn btn-primary" href="#">Read More</a>
                                        </div>
                                    </div>
                                </Col>
                                <Col md={6}>
                                    <div className="blog-box">
                                        <div className="blog-img">
                                            <Image src={process.env.PUBLIC_URL + '/images/blog4.png'} fluid />
                                        </div>
                                        <div className="blog-box-text">
                                            <h4>About International Background Checking...</h4>
                                            <div className="border-class">
                                                <span>By <strong>Admin</strong></span><br/>
                                                <span>November 06, 2019</span>
                                            </div>
                                            <p>When your business spreads its wings to overseas, it comes across multiple challenges. Establishing a new branch is a complicated task on its own and the worries of hiring new employees invite more co...</p>
                                            <a className="btn btn-primary" href="#">Read More</a>
                                        </div>
                                    </div>
                                </Col>
                            </Row>
                        </Container>
                    </div>
                </section>
            </div>
        );
    }
}

export default BlogListing;
