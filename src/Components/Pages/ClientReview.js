import React, { Component, useContext } from "react";
import { EqualHeight, EqualHeightElement } from 'react-equal-height';
import { Container, Row, Col, Image } from 'react-bootstrap';
import { Helmet } from "react-helmet";
import Banner from 'react-js-banner';

class ClientReview extends Component {
    render() {
        return (
            <div>
                <Helmet>
                    <title>Verification Canada | Client Review</title>
                </Helmet>
                <Banner
                    className="inner-banner"
                    title="Client Reviews"
                />
                <section>
                    <div className="client">
                        <Container>
                            <EqualHeight>
                                <Row>
                                    <Col lg={6}>
                                        <div className="review">
                                            <Image src={process.env.PUBLIC_URL + '/images/testi-quote.png'} fluid />
                                            <EqualHeightElement name="Name">
                                                <p>Being that we work with a variety of government departments and environments, a reputable firm exactly what we got when we chose VerificationsCanada for processing our background checks. We work with speaking coaches and personality trainers who are working with different individuals on a daily basis, and so we need to know that our clients and fellow co-workers are in a safe environment. Whenever we bring on a new coach or trainer, we connect with our VerificationsCanada representative and they walk us through every step of the process. We rely heavily on the knowledge of the verification specialists at VerificationsCanada to produce results in a timely manner with superb accuracy. We have never been disappointed.</p>
                                            </EqualHeightElement>
                                            <div className="testi-profile">
                                                <div className="fixed-img">
                                                    <Image src={process.env.PUBLIC_URL + '/images/testi-profile.png'} fluid />
                                                </div>
                                                <div class="block-text">
                                                    <h5>Steve Hopkinson</h5>
                                                    <h6>President</h6>
                                                    <h4>Speakers Momentum</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </Col>
                                    <Col lg={6}>
                                        <div className="review">
                                            <Image src={process.env.PUBLIC_URL + '/images/testi-quote.png'} fluid />
                                            <EqualHeightElement name="Name">
                                                <p>In our business, our people are on the road – a lot! We have multiple trucking companies in which criminal background checks are performed twice a year on drivers and in-house employees. We are moving precious cargo and need to ensure that those in our employment have no history of negligence or worse. The trucking business is fast-paced and drivers are always on the go, and that is why VerificationsCanada is the perfect choice for us. It’s fast, easily accessible from anywhere, dependable, and produced accurate results.</p>
                                            </EqualHeightElement>
                                            <div className="testi-profile">
                                                <div className="fixed-img">
                                                    <Image src={process.env.PUBLIC_URL + '/images/testi-profile.png'} fluid />
                                                </div>
                                                <div class="block-text">
                                                    <h5>Michael Forester</h5>
                                                    <h6>Management</h6>
                                                    <h4>FGI Logistics</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </Col>
                                    <Col lg={6}>
                                        <div className="review">
                                            <Image src={process.env.PUBLIC_URL + '/images/testi-quote.png'} fluid />
                                            <EqualHeightElement name="Name">
                                                <p>Safety for our players, coaches and volunteers is essential to running a fun and winning club. Every year, we always choose VerificationsCanada to manage our criminal background checks for our adult house leagues, coaches, referees, volunteers and parents. The criminal background check is easy and having everything online makes the process accessible by everyone involved. We have been using VerificationsCanada for more than 2 years and we have always had excellent service, and results!</p>
                                            </EqualHeightElement>
                                            <div className="testi-profile">
                                                <div className="fixed-img">
                                                    <Image src={process.env.PUBLIC_URL + '/images/testi-profile.png'} fluid />
                                                </div>
                                                <div class="block-text">
                                                    <h5>Frank</h5>
                                                    <h6>President</h6>
                                                    <h4>Clarkson Soccer Association</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </Col>
                                    <Col lg={6}>
                                        <div className="review">
                                            <Image src={process.env.PUBLIC_URL + '/images/testi-quote.png'} fluid />
                                            <EqualHeightElement name="Name">
                                                <p>Working with multiple organizations to verify employment records of hundreds of potential hires, accuracy and efficiency is top of mind when we were searching for and evaluating a reliable employment verification service. We wanted a firm that was easy to use, cost-effective, produced a speedy turnaround time for results, reliable and well-known in the country. VerificationsCanada hit the mark every time and we are happy with our decision to work with the verification specialists in Canada.</p>
                                            </EqualHeightElement>
                                            <div className="testi-profile">
                                                <div className="fixed-img">
                                                    <Image src={process.env.PUBLIC_URL + '/images/testi-profile.png'} fluid />
                                                </div>
                                                <div class="block-text">
                                                    <h5>President</h5>
                                                    <h6>CEO</h6>
                                                    <h4>Lumbermen’s Credit Services</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </Col>
                                    <Col lg={6}>
                                        <div className="review">
                                            <Image src={process.env.PUBLIC_URL + '/images/testi-quote.png'} fluid />
                                            <EqualHeightElement name="Name">
                                                <p>To buy sell and trade never became easy for our users until we partnered with VerificationsCanada. No other Classifieds will compare to ours now that we have verified users as our main criteria and compliance. Thanks to their team we will continue to use their services as we becomes Canada's #1 Verified Classifieds site.</p>
                                            </EqualHeightElement>
                                            <div className="testi-profile">
                                                <div className="fixed-img">
                                                    <Image src={process.env.PUBLIC_URL + '/images/testi-profile.png'} fluid />
                                                </div>
                                                <div class="block-text">
                                                    <h5>Coneisha Mclean</h5>
                                                    <h6>President & CEO</h6>
                                                    <h4>Grazil, Free Advertising Made Easy</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </Col>
                                    <Col lg={6}>
                                        <div className="review">
                                            <Image src={process.env.PUBLIC_URL + '/images/testi-quote.png'} fluid />
                                            <EqualHeightElement name="Name">
                                                <p>GiYo Properties is a full service property management company, as such we screen through a variety of potential clients for our customers. VerificationsCanada has been pivotal in helping us screen through top noch renters in a friendly, timely manner.</p>
                                            </EqualHeightElement>
                                            <div className="testi-profile">
                                                <div className="fixed-img">
                                                    <Image src={process.env.PUBLIC_URL + '/images/testi-profile.png'} fluid />
                                                </div>
                                                <div class="block-text">
                                                    <h5>Yandi Martelly</h5>
                                                    <h6>CEO</h6>
                                                    <h4>GiYo Properties</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </Col>
                                </Row>
                            </EqualHeight>
                        </Container>
                    </div>
                </section>
                <div className="review-blocks">
                    <section>
                        <Container>
                            <Row>
                                <Col md={4}>
                                    <div className="click-block">
                                        <h4>Employers</h4>
                                        <p>For Corporate Login or Pricing</p>
                                        <a className="btn btn-primary" href="#">Click Here Now</a>
                                    </div>
                                </Col>
                                <Col md={4}>
                                    <div className="click-block">
                                        <h4>Get a Quote</h4>
                                        <p>For Corporate Pricing </p>
                                        <a className="btn btn-primary" href="#">Click Here Now</a>
                                    </div>
                                </Col>
                                <Col md={4}>
                                    <div className="click-block">
                                        <h4>Individuals</h4>
                                        <p>To Start Your Background</p>
                                        <a className="btn btn-primary" href="#">Click Here Now</a>
                                    </div>
                                </Col>
                                <Col md={12}>
                                    <div className="review-block-text">
                                        <p>By partnering with accredited vendors, Verifications Canada Fingerprinting Services allow you to feel secure and protected with the highest level of safeguarding for you, your potential employee, and your business. Verifications Canada, we offer customized solutions through our partnerships with world leaders in digital security, identification and authentication. Our Enhanced Criminal Background
                                            Checks service is the perfect example of providing clients with a complete and comprehensive report
                                            from using the latest digital security and database technology.</p>
                                    </div>
                                </Col>
                            </Row>
                        </Container>
                    </section>
                </div>
            </div>
        );
    }
}

export default ClientReview;
