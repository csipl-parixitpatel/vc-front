import { Container, Row, Col } from 'react-bootstrap';
import { Helmet } from "react-helmet";
import React from 'react';
import Banner from 'react-js-banner';
import Map from "../ContactComponents/Map";
import ContactForm from "../ContactComponents/ContactForm";
import ContactHere from "../ContactComponents/ContactHere";

class ValidationForm extends React.Component {

    render() {
        return (
            <div>
                <Helmet>
                    <title>Verification Canada | Contact Us</title>
                </Helmet>
                <Banner
                    className="inner-banner"
                    title="Contact Us"
                />
                <section>
                    <Container>
                        <ContactHere />
                        <div className="form-box">
                            <Row>
                                <Col md={6} className="pr-0">
                                    <Map />
                                </Col>
                                <Col md={6} className="pl-0">
                                    <ContactForm />
                                </Col>
                            </Row>
                        </div>
                    </Container>
                </section>
            </div>
        );
    }
}
export default ValidationForm;
