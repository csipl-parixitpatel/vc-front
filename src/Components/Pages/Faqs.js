import React, { useState } from 'react';
import { Container, Row, Col, Accordion } from 'react-bootstrap';
import { Helmet } from "react-helmet";
import Banner from 'react-js-banner';

function Faqs() {

    const [activeId, setActiveId] = useState('0');

    function toggleActive(id) {
        if (activeId === id) {
            setActiveId(null);
        } else {
            setActiveId(id);
        }
    }

    return (
        <div>
            <Helmet>
                <title>Verification Canada | FAQs</title>
            </Helmet>
            <Banner
                className="inner-banner"
                title="Frequently Asked Questions"
            />
            <section>
                <div className="faq">
                    <Container>
                        <Row>
                            <Col lg={12}>
                                <Accordion defaultActiveKey={activeId}>
                                    <div className={activeId === '0' ? 'panel-wrap active-panel' : 'panel-wrap'}>
                                        <div className="panel-header">
                                            <Accordion.Toggle onClick={() => toggleActive('0')} className="panel-toggle" eventKey="0">
                                                Are pre-employment background checks common?
                                            </Accordion.Toggle>
                                        </div>
                                        <Accordion.Collapse eventKey="0">
                                            <div className="panel-body">
                                                The short answer is yes. Today, close to 80% of all employers, no matter the size of staff or revenue, perform a pre-employment background check on candidates for hire. This need for security and safety has increased ever since 9/11, and related threats, and online data exposure incidents. Pre-employment background checks should always be conducted accurately and professionally and comply with privacy laws and regulations. It is important to always gain consent prior to performing a pre-employment background check on a candidate.
                                            </div>
                                        </Accordion.Collapse>
                                    </div>
                                    <div className={activeId === '1' ? 'panel-wrap active-panel' : 'panel-wrap'}>
                                        <div className="panel-header">
                                            <Accordion.Toggle onClick={() => toggleActive('1')} className="panel-toggle" eventKey="1">
                                                What is a pre-employment background check
                                            </Accordion.Toggle>
                                        </div>
                                        <Accordion.Collapse eventKey="1">
                                            <div className="panel-body">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                            </div>
                                        </Accordion.Collapse>
                                    </div>
                                </Accordion>
                            </Col>
                        </Row>
                    </Container>
                </div>
            </section>
        </div>
    );
}


export default Faqs;
