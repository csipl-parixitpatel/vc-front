import { Container, Row, Col, Image } from 'react-bootstrap';
import { React, Component } from "react";
import { Helmet } from "react-helmet";
import Banner from 'react-js-banner';
import GetQuoteBg from "../GetQuoteComponents/GetQuoteBg";
import ReliableBgCheck from "../GetQuoteComponents/ReliableBgCheck";

class GetQuote extends Component {

    render() {
        return (
            <div>
                <Helmet>
                    <title>Verification Canada | Get a Quote</title>
                </Helmet>
                <Banner
                    className="inner-banner"
                    title="Partner With Us"
                />
                <section>
                    <Container>
                        <div class="quote">
                            <h1 className="color-darkblue text-center">Reliable Long Term Solutions for Your Business</h1>
                            <Row>
                                <Col md={6}>
                                    <div className="quote-img">
                                        <Image src={process.env.PUBLIC_URL + '/images/quote-image1.png'} fluid />
                                    </div>
                                </Col>
                                <Col md={6}>
                                    <div class="quote-text">
                                        <p>Whether you are a business owner, or an executive for your company, we understand that</p>
                                        <p>it is important that you bring the most suitable talent into your team. At VerificationsCanada, we partner with companies from all industries and all sizes and</p>
                                        <p>provide customizable solutions based on your needs.</p>
                                        <p>Our consultants are looking forward to tell you more about our services and pricing.</p>
                                        <a className="btn btn-primary" href="#">get a quote</a>
                                    </div>
                                </Col>
                            </Row>
                        </div>
                    </Container>
                </section>
                <GetQuoteBg />
                <ReliableBgCheck />
            </div>
        );
    }
}
export default GetQuote;
