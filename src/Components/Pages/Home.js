import React, { Component } from "react";
import { Helmet } from "react-helmet";
import HomeBanner from '../HomeBanner/HomeBanner';
import Compliance from '../Compliance/Compliance';
import FollowSteps from '../FollowSteps/FollowSteps';
import WhyVC from '../WhyVC/WhyVC';
import OurPartners from '../OurPartners/OurPartners';

class Home extends Component {
    render() {
        return (
            <div>
                <Helmet>
                    <title>Verification Canada</title>
                </Helmet>
                <HomeBanner />
                <Compliance />
                <FollowSteps />
                <WhyVC />
                <OurPartners />
            </div>
        );
    }
}

export default Home;
