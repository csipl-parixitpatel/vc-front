import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import { React, Component } from "react";
import { Helmet } from "react-helmet";
import Banner from 'react-js-banner';
import Recaptcha from "react-recaptcha";
import { Typeahead } from 'react-bootstrap-typeahead';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import options from '../QuotationData/IndustryData';
import options1 from '../QuotationData/ProvinceData';


class Quotation extends Component {
    constructor(props) {
        super(props);

        this.recaptchaLoaded = this.recaptchaLoaded.bind(this);
    }
    recaptchaLoaded() {
        console.log('Captcha Successfull !')
    }
    render() {
        return (
            <div>
                <Helmet>
                    <title>Verification Canada | Quotation</title>
                </Helmet>
                <Banner
                    className="inner-banner"
                    title="Request a Quote for Your Business"
                />
                <section>
                    <Container>
                        <div class="quotation">
                            <h1 className="color-darkblue text-center">Personal Information</h1>
                            <Row>
                                <Col md={6}>
                                    <Form.Group>
                                        <Form.Control type="text" placeholder="Full Name" />
                                    </Form.Group>
                                </Col>
                                <Col md={6}>
                                    <Form.Group>
                                        <Form.Control type="email" placeholder="Email Address" />
                                    </Form.Group>
                                </Col>
                                <Col md={6}>
                                    <Form.Group>
                                        <Form.Control type="text" placeholder="Contact Number" />
                                    </Form.Group>
                                </Col>
                                <Col md={6}>
                                    <Form.Group>
                                        <Form.Control type="text" placeholder="Company Name" />
                                    </Form.Group>
                                </Col>
                                <Col md={6}>
                                    <Form.Group>
                                        <Form.Control type="text" placeholder="Job Title" />
                                    </Form.Group>
                                </Col>
                                <Col md={6}>
                                    <Form.Group>
                                        <Form.Control type="text" placeholder="Province" />
                                    </Form.Group>
                                </Col>
                                <Col md={6}>
                                    <Form.Group>
                                        <Typeahead
                                            id="basic-example"
                                            options={options}
                                            placeholder="Industries"
                                        />
                                    </Form.Group>
                                </Col>
                                <Col md={6}>
                                    <Form.Group>
                                        <Typeahead
                                            id="basic-example2"
                                            options={options1}
                                            placeholder="Province"
                                        />
                                    </Form.Group>
                                </Col>
                                <Col md={12}>
                                    <Form.Group controlId="MessageTextarea">
                                        <Form.Control as="textarea" rows={5} placeholder="Message" name="message" />
                                    </Form.Group>
                                </Col>
                                <Col md={12}>
                                    <Form.Group className="text-center">
                                        <Recaptcha
                                            sitekey="6Lfmya4bAAAAAHf-bK-XrPfCWXGCuX5zKUNyN073"
                                            render="explicit"
                                            onloadCallback={this.recaptchaLoaded}
                                        />
                                    </Form.Group>
                                </Col>
                                <Col md={12}>
                                    <div className="text-center">
                                        <Button variant="outline-primary" type="submit">
                                            Send Quote Request
                                        </Button>
                                    </div>
                                </Col>
                            </Row>
                        </div>
                    </Container>
                </section>
            </div>
        );
    }
}
export default Quotation;
