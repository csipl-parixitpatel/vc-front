import React, { Component } from "react";
import { Container, Row, Col, Image } from 'react-bootstrap';
import OwlCarousel from "react-owl-carousel";

class BlogDetail extends Component {
    state = {
        nav: true,
        navText: ["<div class='nav-btn prev-slide'></div>", "<div class='nav-btn next-slide'></div>"],
        responsive: {
            0: {
                items: 1,
            },
            450: {
                items: 1,
            },
            600: {
                items: 2,
            },
            1000: {
                items: 3,
            },
        },
    }
    render() {
        return (
            <div>
                <Container>
                    <Row>
                        <Col lg={12}>
                            <div className="recent-blogs">
                                <h1 className="color-darkblue text-center">Recent Blogs</h1>
                                <OwlCarousel className="owl-theme" loop margin={10} nav responsive={this.state.responsive} autoplay={true}>
                                    <div className="item">
                                        <div className="blog-box">
                                            <div className="blog-img">
                                                <Image src={process.env.PUBLIC_URL + '/images/rec1.png'} fluid />
                                            </div>
                                            <div className="blog-box-text">
                                                <p>Fingerprints Background Checks – What You Need T...</p>
                                                <div className="border-class">
                                                    <span>By <strong>Admin</strong></span><br />
                                                    <span>November 06, 2019</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="item">
                                        <div className="blog-box">
                                            <div className="blog-img">
                                                <Image src={process.env.PUBLIC_URL + '/images/rec2.png'} fluid />
                                            </div>
                                            <div className="blog-box-text">
                                                <p>The Ultimate Guide To Employment Verification Chec...</p>
                                                <div className="border-class">
                                                    <span>By <strong>Admin</strong></span><br />
                                                    <span>November 06, 2019</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="item">
                                        <div className="blog-box">
                                            <div className="blog-img">
                                                <Image src={process.env.PUBLIC_URL + '/images/rec3.png'} fluid />
                                            </div>
                                            <div className="blog-box-text">
                                                <p>The Ultimate Guide To Employment Verification Chec...</p>
                                                <div className="border-class">
                                                    <span>By <strong>Admin</strong></span><br />
                                                    <span>November 06, 2019</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </OwlCarousel>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

export default BlogDetail;
