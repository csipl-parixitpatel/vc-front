import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from '../Pages/Home';
import About from '../Pages/About';
import Faqs from '../Pages/Faqs';
import BlogListing from '../Pages/BlogListing';
import BlogDetail from '../Pages/BlogDetail';
import ContactUs from '../Pages/ContactUs';
import GetQuote from '../Pages/GetQuote';
import Quotation from '../Pages/Quotation';
import ClientReview from '../Pages/ClientReview';
import Login from '../Auth/Login';
import Register from '../Auth/Register';
import Orders from '../Dashboard/Orders';
import Invoice from '../Dashboard/Invoice';
import Addresses from '../Dashboard/Addresses';
import AccountDetails from '../Dashboard/AccountDetails';
import ChangePassword from '../Dashboard/ChangePassword';
import ForgotPassword from '../Auth/ForgotPassword';

import { Redirect } from 'react-router';

export default function index() {
  return (
      <Router>
        <div>
          <Switch>
            <Route exact path="/">
              <Redirect to="/Home" />
            </Route>
            <Route path="/Home">
              <Home />
            </Route>
            <Route path="/About">
              <About />
            </Route>
            <Route path="/Faqs">
              <Faqs />
            </Route>
            <Route path="/BlogListing">
              <BlogListing />
            </Route>
            <Route path="/BlogDetail">
              <BlogDetail />
            </Route>
            <Route path="/ContactUs">
              <ContactUs />
            </Route>
            <Route path="/GetQuote">
              <GetQuote />
            </Route>
            <Route path="/Quotation">
              <Quotation />
            </Route>
            <Route path="/ClientReview">
              <ClientReview />
            </Route>
            <Route path="/Login">
              <Login />
            </Route>
            <Route path="/Register">
              <Register />
            </Route>
            <Route path="/Orders">
              <Orders />
            </Route>
            <Route path="/Invoice">
              <Invoice />
            </Route>
            <Route path="/Addresses">
              <Addresses />
            </Route>
            <Route path="/AccountDetails">
              <AccountDetails />
            </Route>
            <Route path="/ChangePassword">
              <ChangePassword />
            </Route>
            <Route path="/ForgotPassword">
              <ForgotPassword />
            </Route>
          </Switch>
        </div>
      </Router>
  );
}
