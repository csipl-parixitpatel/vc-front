import { Container, Row, Col, Image, Button } from 'react-bootstrap';

function WhyVC() {
    return (
        <div className="why-vc">
            <Container>
                <Row>
                    <Col md={12}>
                        <div className="why">
                            <h1 className="text-center color-darkblue">WHY USE VerificationsCanada.ca?</h1>
                            <Row>
                                <Col lg={{ span: 5, offset: 1 }} md={6}>
                                    <div className="why-box">
                                        <Image src={process.env.PUBLIC_URL + '/images/com-1.png'} fluid />
                                        <p>No Confusing Forms</p>
                                    </div>
                                </Col>
                                <Col lg={{ span: 5, offset: 1 }} md={6}>
                                    <div className="why-box">
                                        <Image src={process.env.PUBLIC_URL + '/images/com-2.png'} fluid />
                                        <p>No Hidden Fees</p>
                                    </div>
                                </Col>
                                <Col lg={{ span: 5, offset: 1 }} md={6}>
                                    <div className="why-box">
                                        <Image src={process.env.PUBLIC_URL + '/images/com-3.png'} fluid />
                                        <p>No Police Station Visits</p>
                                    </div>
                                </Col>
                                <Col lg={{ span: 5, offset: 1 }} md={6}>
                                    <div className="why-box">
                                        <Image src={process.env.PUBLIC_URL + '/images/com-4.png'} fluid />
                                        <p>Recieve Results within<br />15 minutes to an hour</p>
                                    </div>
                                </Col>
                            </Row>
                            <div className="text-center">
                                <Button variant="primary" className="bg-blue">Get my check now</Button>
                            </div>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}

export default WhyVC;
